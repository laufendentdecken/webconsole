package at.running.frontend.webconsole.controller.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Pipeline {
	private String name;
	private String content;
}
