package at.running.frontend.webconsole.controller;

import static org.springframework.http.HttpStatus.OK;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;

import at.running.frontend.webconsole.controller.model.Help;
import at.running.frontend.webconsole.exception.NotKnownCommand;
import at.running.frontend.webconsole.utils.SystemUtils;
import at.running.frontend.webconsole.utils.UrlUtils;
import lombok.Getter;
import lombok.Setter;

@Controller
public class ConsoleController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	@Setter
	@Getter
	private UrlUtils urlUtils;

	@Autowired
	@Setter
	private SystemUtils systemUtils;

	@PostMapping("/console")
	@ResponseBody
	public String index(String line) {
		String result = "";
		String pipeline = null;

		for (String command : line.split("\\|")) {
			String[] cmd = StringUtils.split(command, " ");

			String name = urlUtils.getName(cmd);
			if (systemUtils.isNotKnown(name)) {
				return command + " is not known";
			}

			try {
				verifyCommand(cmd);
				result = executeCommand(cmd, pipeline);
				pipeline = createPipline(name, result);
			} catch (NotKnownCommand nkc) {
				return nkc.getMessage();
			} catch (Exception e) {
				e.printStackTrace();
				return "Error executing command";
			}

		}
		return result;
	}

	private String createPipline(String name, String result) {
		return "{\"pipeline\":{\"name\":\"" + name + "\",\"content\":" + result + "}}";
	}

	private String executeCommand(String[] cmd, String pipeline) {
		String params = "";
		if (Arrays.stream(cmd).filter(c -> c.contains("--")).count() > 0) {
			params = urlUtils.buildParam(cmd);
		}
		String url = urlUtils.getUrl(cmd) + urlUtils.buildPath(cmd) + params;

		HttpEntity<String> requestEntity = null;
		HttpMethod method = null;
		if (!StringUtils.isEmpty(pipeline)) {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("Accept", "*/*");
			requestEntity = new HttpEntity<String>(pipeline, headers);
			method = HttpMethod.POST;
		} else {
			method = HttpMethod.GET;
		}
		ResponseEntity<String> commandResponse = restTemplate.exchange(url, method, requestEntity, String.class);

		if (isReponseOk(commandResponse)) {
			return commandResponse.getBody();
		}

		return "Unkown Error";
	}

	private void verifyCommand(String[] cmd) throws NotKnownCommand {
		ResponseEntity<List<Help>> response = restTemplate.exchange(urlUtils.getUrl(cmd) + cmd[0] + "/help",
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Help>>() {
				});

		List<Help> helpers = response.getBody();
		if (!helpers.stream().anyMatch(a -> a.getArg().equals(cmd[1]))) {
			throw new NotKnownCommand(
					cmd[1] + " is not a known command. Currently available are: " + listArguments(response));
		}
		Help help = findHelp(cmd, helpers);
		List<String> params = extractParamNames(cmd);

		StringBuilder b = new StringBuilder();
		params.stream().forEach(p -> {
			if (!help.getParams().contains(p)) {
				b.append(p + " not valid argument");
			}
		});

		if (b.toString().length() > 0) {
			throw new NotKnownCommand(b.toString());
		}
	}

	private List<String> extractParamNames(String[] cmd) {
		List<String> params = urlUtils.getParams(cmd);
		params.replaceAll(a -> a.replace("--", ""));
		params = params.stream().map(p -> p.substring(0, p.indexOf("="))).collect(Collectors.toList());
		return params;
	}

	private Help findHelp(String[] cmd, List<Help> help) {
		return help.stream().filter(h -> h.getArg().equals(cmd[1])).findAny().orElse(null);
	}

	private StringBuilder listArguments(ResponseEntity<List<Help>> response) {
		StringBuilder b = new StringBuilder();
		response.getBody().forEach(e -> b.append("| " + e.getArg() + " |"));
		return b;
	}

	private boolean isReponseOk(ResponseEntity<String> commandResponse) {
		return commandResponse.getStatusCode().equals(OK);
	}
}