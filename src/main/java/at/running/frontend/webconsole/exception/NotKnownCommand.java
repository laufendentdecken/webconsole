package at.running.frontend.webconsole.exception;

public class NotKnownCommand extends Exception {

	private static final long serialVersionUID = 1L;

	public NotKnownCommand(String message) {
		super(message);
	}
}
