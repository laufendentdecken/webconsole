package at.running.frontend.webconsole.utils;

import static org.thymeleaf.util.StringUtils.join;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Setter;

@Service
public class UrlUtils {
	
	@Autowired
	@Setter
	private SystemUtils systemUtils;

	public String getUrl(String[] cmd) {
		return "http://" + systemUtils.getEnv(getName(cmd))  + "/";
	}

	public String getName(String[] cmd) {
		return cmd[0].toUpperCase();
	}
	
	public String buildPath(String[] cmd) {
		return join(Arrays.stream(cmd).filter(a -> !a.contains("--")).toArray(String[]::new), "/");
	}
	
	public String buildParam(String[] cmd) {
		List<String> list = getParams(cmd);
		list.replaceAll(a -> a.replace("--", "&"));
		String params = join(list, "");
		return "?" + params.substring(1, params.length());
	}

	public List<String> getParams(String[] cmd) {
		return Arrays.stream(cmd).filter(a -> a.contains("--") && a.contains("=")).collect(Collectors.toList());
	}

	
}
