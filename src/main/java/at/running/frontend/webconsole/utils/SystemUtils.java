package at.running.frontend.webconsole.utils;

import static java.lang.System.getenv;

import org.springframework.stereotype.Service;

@Service
public class SystemUtils {

	public boolean isNotKnown(String command) {
		return getEnv(command) == null;
	}

	public String getEnv(String command) {
		return getenv(command + "_SERVICE");
	}
}
