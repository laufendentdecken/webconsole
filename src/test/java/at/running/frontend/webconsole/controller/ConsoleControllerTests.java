package at.running.frontend.webconsole.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.support.RestGatewaySupport;

import at.running.frontend.webconsole.utils.SystemUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsoleControllerTests {
	
	@Autowired
    private RestTemplate restTemplate;

	@Autowired
	private ConsoleController consoleController;
	
	private MockRestServiceServer mockServer;

	private SystemUtils mockUtils;
	
	@Before
    public void setUp() throws Exception {
        RestGatewaySupport gateway = new RestGatewaySupport();
        gateway.setRestTemplate(restTemplate);
        mockServer = MockRestServiceServer.createServer(gateway);
    
        mockUtils = mock(SystemUtils.class);
        when(mockUtils.getEnv(Mockito.anyString())).thenReturn(null);
        doReturn("localhost:8080").when(mockUtils).getEnv("STRAVA");
        doReturn("localhost:8080").when(mockUtils).getEnv("FTP");
        
        
        when(mockUtils.isNotKnown(Mockito.anyString())).thenReturn(true);
        doReturn(false).when(mockUtils).isNotKnown("STRAVA");
        doReturn(false).when(mockUtils).isNotKnown("FTP");
        
        consoleController.setSystemUtils(mockUtils);
        
		consoleController.getUrlUtils().setSystemUtils(mockUtils);
        
	}

	@Test
	public void listOneEvent() {
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/help")).andRespond(withSuccess("[{\"arg\":\"get\",\"params\":[]},{\"arg\":\"list\",\"params\":[\"arg\"]}]", MediaType.APPLICATION_JSON));
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/list/1")).andRespond(withSuccess("123 Test", MediaType.TEXT_PLAIN));
		String response = consoleController.index("strava list 1");
		assertEquals("123 Test", response);
     	mockServer.verify();
	}
	
	@Test
	public void listOneEventOnlyId() {
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/help")).andRespond(withSuccess("[{\"arg\":\"get\",\"params\":[]},{\"arg\":\"list\",\"params\":[\"arg\"]}]", MediaType.APPLICATION_JSON));
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/list/1?arg=id")).andRespond(withSuccess("123", MediaType.TEXT_PLAIN));
		String response = consoleController.index("strava list 1 --arg=id");
		assertEquals("123", response);
     	mockServer.verify();
	}
	
	@Test
	public void NotValidStravaArgument() {
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/help")).andRespond(withSuccess("[{\"arg\":\"get\",\"params\":[]},{\"arg\":\"list\",\"params\":[\"arg\"]}]", MediaType.APPLICATION_JSON));
		String response = consoleController.index("strava list 1 --arg1=id");
		assertEquals("arg1 not valid argument", response);
     	mockServer.verify();
	}
	
	@Test
	public void NotValidStravaCommand() {
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/help")).andRespond(withSuccess("[{\"arg\":\"get\",\"params\":[]},{\"arg\":\"list\",\"params\":[\"arg\"]}]", MediaType.APPLICATION_JSON));
		String response = consoleController.index("strava notvalid");
		assertTrue(response.contains("notvalid is not a known command."));
     	mockServer.verify();
	}
	
	@Test
	public void NotValidCommand() {
		String response = consoleController.index("NotValid");
		assertEquals("NotValid is not known", response);
     	mockServer.verify();
	}
	
	@Test
	public void piplinetoFTPTest() {
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/help")).andRespond(withSuccess("[{\"arg\":\"get\",\"params\":[]},{\"arg\":\"list\",\"params\":[\"arg\"]}]", MediaType.APPLICATION_JSON));
		mockServer.expect(once(), requestTo("http://localhost:8080/strava/list/1?arg=id")).andRespond(withSuccess("{\"id\":123}", MediaType.TEXT_PLAIN));
		mockServer.expect(once(), requestTo("http://localhost:8080/ftp/help")).andRespond(withSuccess("[{\"arg\":\"calc\",\"params\":[]}]", MediaType.APPLICATION_JSON));
		mockServer.expect(once(), requestTo("http://localhost:8080/ftp/calc")).andExpect(content().json("{\"pipeline\":{\"name\":\"STRAVA\",\"content\":{\"id\":123}}}")).andRespond(withSuccess("285", MediaType.TEXT_PLAIN));
		String response = consoleController.index("strava list 1 --arg=id | ftp calc");
		mockServer.verify();
		assertEquals("285", response);
	}
	
	@Test
	public void testFTPCalc() {
		mockServer.expect(once(), requestTo("http://localhost:8080/ftp/help")).andRespond(withSuccess("[{\"arg\":\"calc\",\"params\":[]}]", MediaType.APPLICATION_JSON));
		mockServer.expect(once(), requestTo("http://localhost:8080/ftp/calc")).andRespond(withSuccess("285", MediaType.TEXT_PLAIN));
		String response = consoleController.index("ftp calc");
		mockServer.verify();
		assertEquals("285", response);
	}
	
}
